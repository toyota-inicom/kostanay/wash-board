<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWashesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('washes', function (Blueprint $table) {
            $table->id();
            $table->string('car_number');
            $table->string('car_vin');
            $table->dateTime('start_times');
            $table->string('zn_number');
            $table->string('car_wash');
            $table->string('status');
            $table->string('where_key');
            $table->string('post');
            $table->string('destination');
            $table->string('quality_control');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('washes');
    }
}
