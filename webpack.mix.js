const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .sass('resources/sass/app.scss', 'public/css')
    .browserSync({
        proxy: 'wash.loc',
        files: [
            'resources/**/*.js',
            'resources/**/*.scss',
            'resources/**/*.php',
            'resources/**/*.vue',
        ]
    });
