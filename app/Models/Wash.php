<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wash extends Model
{
    use HasFactory;

    protected $fillable = [
        'car_number',
        'car_vin',
        'start_times',
        'zn_number',
        'car_wash',
        'status',
        'where_key',
        'post',
        'destination',
        'quality_control'
    ];

}
