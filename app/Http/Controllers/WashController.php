<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Wash;

class WashController extends Controller
{

    function numberType($number, $vin): array
    {
        $car_data = [];
        if (preg_match('/^[\d]{3}[\D]{3}[\d]{2}$/ui', $number)) { // 123AAA12
            preg_match_all('/^([\d]{3})([\D]{3})([\d]{2})$/ui', $number, $matches);
            $html_num = '<div class="num_img"><div class="kaz_flag"></div><span>KZ</span></div>' .
                '<div class="num_digits">' . $matches[1][0] . '</div>' .
                '<div class="num_letters">' . $matches[2][0] . '</div>' .
                '<div class="num_region">' . $matches[3][0] . '</div>';
            $car_data['number_type'] = 'kz_white';
            $car_data['number_html'] = $html_num;

        } elseif (preg_match('/^[\d]{3}[\D]{2}[\d]{2}$/ui', $number)) { // 123AA12
            preg_match_all('/^([\d]{3})([\D]{2})([\d]{2})$/ui', $number, $matches);
            $html_num = '<div class="num_img"><div class="kaz_flag"></div><span>KZ</span></div>' .
                '<div class="num_digits">' . $matches[1][0] . '</div>' .
                '<div class="num_letters">' . $matches[2][0] . '</div>' .
                '<div class="num_region">' . $matches[3][0] . '</div>';
            $car_data['number_type'] = 'kz_white';
            $car_data['number_html'] = $html_num;

        } elseif (preg_match('/^[\D]{1}[\d]{3}[\D]{3}$/ui', $number)) { // P123ABC
            preg_match_all('/^([\D]{1})([\d]{3})([\D]{3})$/ui', $number, $matches);
            $html_num =
                '<div class="num_digits">' . $matches[1][0] . '</div>' .
                '<div class="num_digits">' . $matches[2][0] . '</div>' .
                '<div class="num_digits">' . $matches[3][0] . '</div>';
            $car_data['number_type'] = 'old_number';
            $car_data['number_html'] = $html_num;

        } elseif (preg_match('/^[\D]{1}[\d]{3}[\D]{2}$/ui', $number)) { // P123AB
            preg_match_all('/^([\D]{1})([\d]{3})([\D]{2})$/ui', $number, $matches);
            $html_num =
                '<div class="num_digits">' . $matches[1][0] . '</div>' .
                '<div class="num_digits">' . $matches[2][0] . '</div>' .
                '<div class="num_digits">' . $matches[3][0] . '</div>';
            $car_data['number_type'] = 'old_number';
            $car_data['number_html'] = $html_num;


        } elseif (preg_match('/^[\D]{1}[\d]{3}[\D]{2}[\d]{2,3}$/ui', $number)) { // E123AB123 / E123AB12
            preg_match_all('/^([\D]{1})([\d]{3})([\D]{2})([\d]{2,3})$/ui', $number, $matches);
            $html_num =
                '<div class="num_letters">' . $matches[1][0] . '</div>' .
                '<div class="num_digits">' . $matches[2][0] . '</div>' .
                '<div class="num_letters">' . $matches[3][0] . '</div>' .
                '<div class="num_region"><span>' . $matches[4][0] . '</span><span class="num_img"></span></div>';
            $car_data['number_type'] = 'rus';
            $car_data['number_html'] = $html_num;

        } elseif (preg_match('/^[\d]{6}$/', $number)) { // 123456
            preg_match_all('/^([\d]{6})$/ui', $number, $matches);
            $html_num =
                '<div class="num_img"><i class="ri-barcode-line"></i></div>' .
                '<div class="num_digits">' . $matches[1][0] . '</div>';
            $car_data['number_type'] = 'vin';
            $car_data['number_html'] = $html_num;

        } elseif (preg_match('/^([\D]{1})([\d]{4})([\d]{2})$/ui', $number)) { // H123456
            preg_match_all('/^([\D]{1})([\d]{4})([\d]{2})$/ui', $number, $matches);
            if ($matches[3][0] == 18 || $matches[3][0] < 18) {  // H1234|18
                $html_num = '<div class="num_img"><div class="kaz_flag"></div><span>KZ</span></div>' .
                    '<div class="num_letters">' . $matches[1][0] . '</div>' .
                    '<div class="num_digits">' . $matches[2][0] . '</div>' .
                    '<div class="num_region">' . $matches[3][0] . '</div>';
                $car_data['number_type'] = 'yellow';
                $car_data['number_html'] = $html_num;
            } else { // H123456
                $html_num = '<div class="num_letters">' . $matches[1][0] . '</div>' .
                    '<div class="num_digits">' . $matches[2][0] . '' . $matches[3][0] . '</div>';
                $car_data['number_type'] = 'not_resident';
                $car_data['number_html'] = $html_num;
            }
        } else {
            if (preg_match('/^[-]{1,}$/ui', $number) || $number == '') {
                $car_vin = substr($vin, -6);
                $html_num =
                    '<div class="num_img"><i class="ri-barcode-line"></i></div>' .
                    '<div class="num_digits">' . $car_vin . '</div>';
                $car_data['number_type'] = 'vin';
                $car_data['number_html'] = $html_num;
            } else {
                $html_num = '<div class="num_digits">' . $number . '</div>';
                $car_data['number_type'] = 'other';
                $car_data['number_html'] = $html_num;
            }
        }
        return $car_data;
    }

    function dataReadFromBase()
    {
        $cars_list = Wash::all();
        $car_all_data = [];

        foreach ($cars_list as $car_num) {

            // Время (Начало, Сейчас, Разница)
            $start_time = Carbon::parse($car_num->start_times);
            $now_time = Carbon::parse(now());
            $diffTime = $start_time->diff($now_time);

            //print_r($diffTime);

            // Номер машины или вин-код
            $car_data['number_type'] = $this->numberType($car_num->car_number, $car_num->car_vin)['number_type'];
            $car_data['number_html'] = $this->numberType($car_num->car_number, $car_num->car_vin)['number_html'];

            // Отбор машин для показа
            if ($diffTime->invert == 1) {    // если не просрочено
                if ($diffTime->d == 0) {     // сегодня
                    if ($diffTime->h == 0) {  // меньше 1 часа
                        if ($diffTime->i < 30) { // меньше 30 минут

                            if ($car_num->status == 'В ремонтной зоне') {
                                if($car_num->post == $car_num->destination) {
                                    $car_data['status_class'] = 'in_process';
                                    $car_data['status_text'] = 'В ПРОЦЕССЕ';
                                } else {
                                    $car_data['status_class'] = '';
                                    $car_data['status_text'] = 'НЕ НА ПОСТУ';
                                }
                            } elseif ($car_num->status == 'Завершена') {
                                $car_data['status_class'] = 'in_process';
                                $car_data['status_text'] = 'ЗАВЕРШЕНА';
                            } elseif ($car_num->status == 'Заявка' || $car_num->status == 'Утверждена') {
                                if ($diffTime->i <= 10) {
                                    $car_data['status_class'] = 'warning';
                                    $car_data['status_text'] = 'ОЖИДАНИЕ';
                                } else {
                                    $car_data['status_class'] = '';
                                    $car_data['status_text'] = 'ОЖИДАНИЕ';
                                }
                            }

                            // Добавляем всё
                            $car_data['start_times'] = Carbon::parse($start_time)->toDateTimeString();
                            $car_data['status'] = $car_num->status;

                            if ($car_num->where_key) {
                                $where_key = $car_num->where_key;
                                $where_key = str_replace('Новые автомобили', 'Нов.авто', $where_key);
                                $where_key = str_replace('Выдача (теплая приемка)', 'Приёмка', $where_key);
                                $where_key = str_replace('Ожидание запчастей', 'Ожидание з/ч', $where_key);
                                $where_key = str_replace('Выдача после ремонта', 'Выд. после рем.', $where_key);
                                $where_key = str_replace('Место выдачи автомобилей с пробегом', 'Б/у авто', $where_key);
                                $car_data['where_key'] = $where_key;
                            } else {
                                $car_data['where_key'] = '--';
                            }
                            if($car_num->status == 'В ремонтной зоне' || $car_num->status == 'Завершена') {
                                $car_data['where_key'] = 'РЗ';
                            }
                            if ($car_num->car_wash == 'Да') {
                                $car_data['destination'] = 'Мойка ' . $car_num->destination;
                            } else {
                                $car_data['destination'] = 'Цех ' . $car_num->destination;
                            }
                            $car_all_data[strtotime($car_num->start_times)] = $car_data;
                        }
                    }
                }
            } else { // просрочено
                if ($diffTime->d == 0) { // сегодня
                    if ($car_num->status == 'В ремонтной зоне') {
                        $car_data['status_class'] = 'in_process';
                        $car_data['status_text'] = 'В ПРОЦЕССЕ';
                    } elseif ($car_num->status == 'Завершена') {
                        $car_data['status_class'] = 'in_process';
                        $car_data['status_text'] = 'ЗАВЕРШЕНА';
                    } elseif ($car_num->status == 'Заявка' || $car_num->status == 'Утверждена') {
                        $car_data['status_class'] = 'expired';
                        $car_data['status_text'] = 'ПРОСРОЧЕНО';
                    }

                    $car_data['start_times'] = Carbon::parse($start_time)->toDateTimeString();
                    $car_data['status'] = $car_num->status;

                    if ($car_num->where_key) {
                        $where_key = $car_num->where_key;
                        $where_key = str_replace('Новые автомобили', 'НА', $where_key);
                        $where_key = str_replace('Выдача (теплая приемка)', 'ВТП', $where_key);
                        $where_key = str_replace('Ожидание запчастей', 'З/Ч', $where_key);
                        $where_key = str_replace('Выдача после ремонта', 'ВПР', $where_key);
                        $where_key = str_replace('Место выдачи автомобилей с пробегом', 'АСП', $where_key);
                        $car_data['where_key'] = $where_key;
                    } else {
                        $car_data['where_key'] = '--';
                    }
                    if($car_num->status == 'В ремонтной зоне' || $car_num->status == 'Завершена') {
                        $car_data['where_key'] = 'РЗ';
                    }
                    if($car_num->destination) {
                        if ($car_num->car_wash == 'Да') {
                            $car_data['destination'] = 'Мойка ' . $car_num->destination;
                        } else {
                            $car_data['destination'] = 'Цех ' . $car_num->destination;
                        }
                    } else {
                        $car_data['destination'] = '--';
                    }
                    $car_all_data[strtotime($car_num->start_times)] = $car_data;
                }
            }
        }
        return json_encode($car_all_data);
    }

    // для 1С
    function dataWriteToBase(Request $request)
    {
        file_put_contents("import_data.txt", $request->getContent(), FILE_APPEND);
        $data = json_decode($request->getContent(), true);

        $car_number =       $data['car_number'];
        $car_vin =          $data['car_vin'];
        $start_times =      Carbon::parse($data['start_times'])->toDateTimeString();
        $zn_number =        $data['zn_number'];
        $car_wash =         $data['car_wash'];
        $status =           $data['status'];
        $where_key =        $data['where_key'];
        $post =             $data['post'];
        $destination =      $data['destination'];
        $quality_control =  $data['quality_control'];
        $delete =           $data['delete'];
        $end_times =        Carbon::parse($data['end_times'])->toDateTimeString();

        /*
        $db_query = [
            'car_number' => $car_number,
            'car_vin' => $car_vin,
            'start_times' => $start_times,
            'zn_number' => $zn_number,
            'car_wash' => $car_wash,
            'status' => $status,
            'where_key' => $where_key,
            'post' => $post,
            'destination' => $destination,
            'quality_control' => $quality_control,
            'delete' => $delete,
            'end_times' => $end_times
        ];
        */


        // Сценарии добавления / обновления / удаления 
        if($delete == 1) {
            // echo "Удаление: По признаку delete = 1";
            Wash::where('car_vin', $car_vin)->delete();
        } elseif ($quality_control == 'Да') {
            // echo "Удаление: По признаку quality_control = Да";
            Wash::where('car_vin', $car_vin)->delete();
        } elseif ($car_wash != 'Да' && $post != '') {
            // echo "Удаление: Мойки нет, Машина уже на посту";
            Wash::where('car_vin', $car_vin)->delete();
        } else {
            Wash::updateOrCreate(
                ['car_vin' => $car_vin],
                [
                    'car_number' => $car_number,
                    'car_vin' => $car_vin,
                    'start_times' => Carbon::parse($start_times)->toDateTimeString(),
                    'zn_number' => $zn_number,
                    'car_wash' => $car_wash,
                    'status' => $status,
                    'where_key' => $where_key,
                    'post' => $post,
                    'destination' => $destination,
                    'quality_control' => $quality_control,
                    'end_times' => Carbon::parse($end_times)->toDateTimeString()
                ]
            );
        }
        return false;
    }
}
