require('./bootstrap');

import { createApp } from 'vue';
import App from './App.vue';
import axios from 'axios';
import 'remixicon/fonts/remixicon.css'

const app = createApp(App);
app.config.globalProperties.$axios = axios;
app.mount('#app');