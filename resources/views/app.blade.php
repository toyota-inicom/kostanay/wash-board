<!DOCTYPE html>
<html lang="en" translate="no">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="google" translate="no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>WASH BOARD</title>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}?{{ time() }}">
</head>
<body>
<div id="app"></div>
<script src="{{ mix('/js/app.js') }}?{{ time() }}"></script>
</body>
</html>
